import Vue from 'vue'
import App from './App'

import store from './store'

Vue.config.productionTip = false

Vue.prototype.$store = store
import uniPopup from '@/components/uni-popup/uni-popup.vue'
Vue.component('uni-popup', uniPopup);

App.mpType = 'app'

const app = new Vue({
	store,
	...App
})
app.$mount()
